<?php
declare(strict_types=1);

namespace App\Domain\Interfaces;

use DateTimeInterface;

interface ScheduleInterface
{
    public function checkTime(DateTimeInterface $currentTime): bool;

    public function isEnabled(): bool;
}
