<?php
declare(strict_types=1);

namespace App\Domain\Feeds\Actions;

use App\Domain\Feeds\Models\Feed;

class PutAction
{
    public function execute(Feed $entity, array $validatedData): Feed
    {
        $entity->fill($validatedData);
        $entity->updated_by = $validatedData['user_id'];
        $entity->save();
        
        return $entity->refresh();
    }
}
