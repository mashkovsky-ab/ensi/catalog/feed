# Ensi Feed

## Резюме

Название: Ensi Feed  
Домен: Feeds  
Назначение: Передача данных в нужном формате и семантике в сторонние сервисы (рекламные системы, маркетплейсы);  
Актуализации информации о товарах, ценах и стоках на сторонних площадках.  

## Разработка сервиса

Инструкцию описывающую разворот, запуск и тестирование сервиса на локальной машине можно найти в отдельном документе в [Confluence](https://greensight.atlassian.net/wiki/spaces/ENSI/pages/362676232/Backend-)

Регламент работы над задачами тоже находится в [Confluence](https://greensight.atlassian.net/wiki/spaces/ENSI/pages/477528081)

## Структура сервиса

Почитать про структуру сервиса можно здесь [здесь](docs/structure.md)

## Среды

### Test

CI: https://jenkins-infra.ensi.tech/job/ensi-stage-1/job/feeds/job/feed/  
URL: https://feeds-master-dev.ensi.tech/docs/swagger  

### Preprod

Отсутствует

### Prod

Отсутствует

## Контакты

Команда поддерживающая данный сервис: https://gitlab.com/groups/greensight/ensi/-/group_members  
Email для связи: mail@greensight.ru

## Лицензия

[Открытая лицензия на право использования программы для ЭВМ Greensight Ecom Platform (GEP)](LICENSE.md).
