<?php
declare(strict_types=1);

namespace Tests\Feature\Feeds\Controllers;

use App\Domain\Feeds\Models\Feed;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Arr;
use Illuminate\Testing\TestResponse;
use Symfony\Component\HttpFoundation\Response;
use Tests\ComponentTestCase;

/**
 * @deprecated migrate to FeedsComponentTest.php
 */
class FeedControllerTest extends ComponentTestCase
{
    private const URL_SUFFIX = '/api/v1/feeds';

    private const USER_ID_WHEN_CREATING = 9;
    private const USER_ID_WHEN_UPDATING = 6;
    private const EXPECTED_AMOUNT_AFTER_INVALID_DATA = 0;
    private const EXPECTED_AMOUNT_AFTER_VALID_DATA = 1;

    /**
     * @dataProvider createEntityDataSet
     */
    public function testCreateFeedEntity(array $data, int $expectedStatus, \Closure $expectedResponse, int $expectedAmount): void
    {
        $response = $this->post(self::URL_SUFFIX, $data);

        $response->assertStatus($expectedStatus);

        $this->assertEquals($expectedResponse(), $this->forgetDatetimeInResponseData($response));

        $this->assertDatabaseCount(Feed::class, $expectedAmount);
    }

    /**
     * @dataProvider putEntityDataSet
     */
    public function testUpdateFeedEntity(array $inputData, int $expectedStatus, array $expectedData): void
    {
        $feed = $this->factoryForModel(Feed::class)->create(['created_by' => self::USER_ID_WHEN_CREATING]);
        $entityId = Response::HTTP_NOT_FOUND === $expectedStatus ? 0 : $feed->id;
        $response = $this->put(sprintf('%s/%d', self::URL_SUFFIX, $entityId), $inputData);

        $response->assertStatus($expectedStatus);

        $this->assertEquals($expectedData, $this->forgetDatetimeInResponseData($response));

        $response->assertJsonFragment($expectedData);
    }

    /**
     * @depends testUpdateFeedEntity
     */
    public function testRemoveFeedEntity(): void
    {
        $response = $this->delete(sprintf('%s/%d', self::URL_SUFFIX, 1));

        $response->assertStatus(Response::HTTP_NO_CONTENT);
    }

    private function getLastFeedEntity(): Feed
    {
        return Feed::query()->latest('id')->first();
    }

    public function putEntityDataSet(): iterable
    {

        // TODO дописать ожидаемые данные
        return [
            'invalid data: nonexistent id' => [
                ['user_id' => self::USER_ID_WHEN_UPDATING],
                Response::HTTP_NOT_FOUND,
                [],
            ],
            'invalid data: required fields are missing' => [
                ['name' => 'fake name'],
                Response::HTTP_BAD_REQUEST,
                [
                    'data' => null,
                    'errors' => [
                        [
                            'code'    => 'ValidationError',
                            'message' => 'Поле user id обязательно для заполнения.',
                        ],
                    ],
                ],
            ],
            'valid data' => [
                [
                    'user_id' => self::USER_ID_WHEN_UPDATING,
                    'name' => 'fake name',
                    'type' => 'fake type',
                ],
                Response::HTTP_OK,
                [],
            ],
        ];
    }

    public function createEntityDataSet(): iterable
    {
        $invalidResult = static function (): array {
            return  [
                'data' => null,
                'errors' => [
                    [
                        'code'    => 'ValidationError',
                        'message' => 'Поле user id обязательно для заполнения.',
                    ],
                    [
                        'code'    => 'ValidationError',
                        'message' => 'Поле Имя обязательно для заполнения.',
                    ],
                    [
                        'code'    => 'ValidationError',
                        'message' => 'Поле type обязательно для заполнения.',
                    ],
                ],
            ];
        };

        $validResult = function (): array {
            return [
                'data' => [
                    'id'         => $this->getLastFeedEntity()->id, //todo переделать
                    'created_by' => self::USER_ID_WHEN_CREATING,
                    'updated_by' => null,
                    'name'       => 'string',
                    'type'       => 'some-type',
                ],
            ];
        };

        return [
            'invalid input data' => [
                [],
                Response::HTTP_BAD_REQUEST,
                $invalidResult,
                self::EXPECTED_AMOUNT_AFTER_INVALID_DATA,
            ],
            'valid input data' => [
                [
                    'user_id' => self::USER_ID_WHEN_CREATING,
                    'name'    => 'string',
                    'type'    => 'some-type',
                ],
                Response::HTTP_CREATED,
                $validResult,
                self::EXPECTED_AMOUNT_AFTER_VALID_DATA,
            ],
        ];
    }

    private function forgetDatetimeInResponseData(TestResponse $response): array
    {
        $responseAsArray = json_decode($response->getContent(), true);

        $this->forgetRecursive($responseAsArray);

        return $responseAsArray;
    }

    private function forgetRecursive(array &$array): void
    {
        foreach ($array as &$value) {
            if (null === $value) {
                continue;
            }

            if (!is_array($value)) {
                continue;
            }

            Arr::forget($value, ['created_at', 'updated_at']);
            $this->forgetRecursive($value);
        }
    }

    protected function factoryForModel(string $modelName): Factory
    {
        return Factory::factoryForModel($modelName);
    }
}
