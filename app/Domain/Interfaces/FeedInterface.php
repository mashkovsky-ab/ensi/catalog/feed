<?php
declare(strict_types=1);

namespace App\Domain\Interfaces;

interface FeedInterface
{
    public function getName(): string;

    public function getSchedule(): ScheduleInterface;

    public function getToExecute(): CommandInterface;
    
    public function getData(): DenormalizedDataInterface;
}
