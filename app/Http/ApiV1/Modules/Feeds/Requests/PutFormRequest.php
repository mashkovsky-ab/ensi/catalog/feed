<?php
declare(strict_types=1);

namespace App\Http\ApiV1\Modules\Feeds\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class PutFormRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'user_id' => ['required', 'integer'],
            'name'    => ['string'],
            'type'    => ['string'],
        ];
    }
}
